import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class BluetoothServiceMock {
  isEnabled() {
    return Observable.of({});
  }

  forceBluetooth() { }

  scanDevices() { }

  connectToDevice() { }

  disconnectFromDevice() {
    return Promise.resolve();
  }

  getDeviceBattery() {
    return Promise.resolve();
  }

  readDevice() { }

  setSensorEmittingState() {
    return Promise.resolve();
  }

  hexToDigiThermo() {
    return { temperature: 10, humidity: 10 };
  }
}

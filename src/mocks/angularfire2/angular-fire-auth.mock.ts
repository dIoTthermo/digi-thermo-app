import { Observable } from 'rxjs/Observable';

export class AngularFireAuthMock {
  static VALID_EMAIL_PASSWORD = { email: 'valid', password: 'valid' };
  static INVALID_EMAIL_PASSWORD = { email: 'invalid', password: 'invalid '};
  static VALID_EMAIL_PASSWORD_CONFIRM = { email: 'valid', password: 'valid', confirmPassword: 'valid' };
  static INVALID_EMAIL_PASSWORD_CONFIRM_MATCH = { email: 'invalid', password: 'invalid', confirmPassword: 'invalid'};
  static INVALID_EMAIL_PASSWORD_CONFIRM_NO_MATCH = { email: 'invalid', password: 'invalid ', confirmPassword: 'invalid1'};

  static FAKE_PROFILE = {
    displayName: 'Fake',
    email: 'fake@email.com',
    emailVerified: true,
    isAnonymous: false,
    photoURL: null,
    providerData: [],
    providerId: '1',
    refreshToken: '1',
    uid: '1',
  };

  authState: Observable<any>;

  auth = {
    createUserWithEmailAndPassword(email: string , password: string): Promise<any> {
      if (email === 'invalid') {
        return Promise.reject('invalid email');
      }
      return Promise.resolve(AngularFireAuthMock.VALID_EMAIL_PASSWORD_CONFIRM);
    },

    signInWithCredential(credential): Promise<any> {
      return Promise.resolve(AngularFireAuthMock.FAKE_PROFILE);
    },

    signInWithEmailAndPassword(email: string, password: string): Promise<any> {
      if (email === 'invalid') {
        return Promise.reject('invalid email');
      }
      return Promise.resolve();
    },

    sendPasswordResetEmail(email: string): Promise<any> {
      if (email === 'invalid') {
        return Promise.reject('invalid email');
      }
      return Promise.resolve();
    },

    signOut(): Promise <any> {
      return Promise.resolve();
    },
  };

}

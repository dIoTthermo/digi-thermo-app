import { ViewControllerMock } from './view-controller.mock';
import { TabsMock } from './tabs.mock';

function newPromise() {
  return new Promise((resolve, reject) => {
    resolve();
  });
}

export class NavControllerMock {
  parent = new TabsMock();

  setRoot() {
    return true;
  }

  push() {
    return newPromise();
  }

  pop() {
    return newPromise();
  }

  getActive() {
    return new ViewControllerMock();
  }
}

export class ViewControllerMock {
  readReady = {
    subscribe() {

    }
  };
  writeReady = {
    subscribe() {

    }
  };

  _setHeader() {
    //
  }

  _setNavbar() {

  }

  _setIONContent() {

  }

  _setIONContentRef() {

  }

  dismiss() {
    // Nothing
  }
}

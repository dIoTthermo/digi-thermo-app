import { NavControllerMock } from './nav-controller.mock';

export class AppMock {
  fakeActiveNav = new NavControllerMock();

  public getActiveNav() {
    return this.fakeActiveNav;
  }
}

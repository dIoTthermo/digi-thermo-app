import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { IonicModule, AlertController, LoadingController, Alert, Loading, ViewController } from 'ionic-angular';
import { StoreModule, Store } from '@ngrx/store';

import { Subscription } from 'rxjs/Subscription';

import { FeedbackModalComponent } from './feedback-modal';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';

import { LoadingControllerMock } from './../../mocks/ionic/loading-controller.mock';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as profile from '../../store/profile/profile.actions';
import * as fromRoot from './../../store/app.reducer';
import { ProfileService } from './../../providers/profile.service';
import { ViewControllerMock } from './../../mocks/ionic/view-controller.mock';

describe('Feedback', function () {
  let _store: Store <RootState> ;
  let fixture: ComponentFixture <FeedbackModalComponent> ;
  let de: DebugElement;
  let component: FeedbackModalComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({

      declarations: [FeedbackModalComponent],
      imports: [
        IonicModule.forRoot(FeedbackModalComponent),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        FormBuilder,
        {
          provide: LoadingController,
          useClass: LoadingControllerMock,
        },
        {
          provide: ViewController,
          useClass: ViewControllerMock,
        },
        {
          provide: Store,
          useValue: new StoreMock(initialState)
        }
      ]
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(FeedbackModalComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('Should create Feedback component', () => {
    expect(component instanceof FeedbackModalComponent).toBeTruthy();
  });

  describe('Feedback form', () => {
    it('should have four input fields', () => {
      const controls = Object.keys(component.feedbackForm.controls);

      expect(controls.length).toBe(4);
    });
  });

  describe('feedbackForm', () => {
    describe('Choose Navigation star rating', () => {
      let formControl: FormControl;
      beforeEach(() => {
        formControl = component.feedbackForm.controls['navigationRating'] as FormControl;
      });

      it('should exist', () => {
        expect(formControl).toBeDefined();
      });

      it('should be required', () => {
        const errors = formControl.errors || {};
        expect(errors['required']).toBeTruthy();
      });
    });

    describe('Navigation comments text box', () => {
      let formControl: FormControl;
      beforeEach(() => {
        formControl = component.feedbackForm.controls['navigationComments'] as FormControl;
      });

      it('should exist', () => {
        expect(formControl).toBeDefined();
      });

      it('should not be required', () => {
        const errors = formControl.errors || {};

        expect(errors['required']).toBeFalsy();
      });
    });

    describe('\'Features to add\' comments text box', () => {
      let formControl: FormControl;
      beforeEach(() => {
        formControl = component.feedbackForm.controls['featuresToAdd'] as FormControl;
      });

      it('should exist', () => {
        expect(formControl).toBeDefined();
      });

      it('should not be required', () => {
        const errors = formControl.errors || {};

        expect(errors['required']).toBeFalsy();
      });
    });

    describe('\'Tips for improvement\' comments text box', () => {
      let formControl: FormControl;
      beforeEach(() => {
        formControl = component.feedbackForm.controls['tipsForImporovement'] as FormControl;
      });

      it('should exist', () => {
        expect(formControl).toBeDefined();
      });

      it('should not be required', () => {
        const errors = formControl.errors || {};

        expect(errors['required']).toBeFalsy();
      });
    });

    describe('Submit button', () => {
      beforeEach(() => {
        de = fixture.debugElement.query(By.css('button[type="submit"]'));
      });

      it('should exist', () => {
        expect(de).toBeDefined();
      });

      it('should dispatch a new profile.FeedbackAction', () => {
        const fakeFeedback = {
          navigationRating: 5,
          navigationComments: 'Comment about navigation',
          featuresToAdd: 'Comment suggesting new features',
          tipsForImporovement: 'Comment with tips for improvement',
        };
        expect(component.feedbackForm.valid).toBeFalsy();

        component.feedbackForm.controls['navigationRating'].setValue(fakeFeedback.navigationRating);
        component.feedbackForm.controls['navigationComments'].setValue(fakeFeedback.navigationComments);
        component.feedbackForm.controls['featuresToAdd'].setValue(fakeFeedback.featuresToAdd);
        component.feedbackForm.controls['tipsForImporovement'].setValue(fakeFeedback.tipsForImporovement);
        expect(component.feedbackForm.valid).toBeTruthy();
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof profile.FeedbackAction).toBeTruthy();
          expect(action.type).toBe(profile.FEEDBACK);
          expect(action.payload).toEqual(fakeFeedback);
        });

        component.sendFeedback();
      });
    });

    describe('disableFeedbackButton$', () => {
      it('should be updated upon a change in the feedback star rating', () => {
        const navigationRating = component.feedbackForm.controls['navigationRating'];

        let testCase = 0;
        component.disableFeedbackButton$.subscribe((disable) => {
          // Initial value
          if (testCase === 0) {
            expect(disable).toBeTruthy();
          }

          // Invalid Form
          if (testCase === 1) {
            expect(disable).toBeTruthy();
          }

          // Invalid Form
          if (testCase === 2) {
            expect(disable).toBeFalsy();
          }
        });

        // Invalid Form
        testCase++;
        navigationRating.setValue('');

        // Valid Form
        testCase++;
        navigationRating.setValue(5);
      });
    });

    describe('subscriptions', () => {
      it('should be created', () => {
        expect(component.subscriptions instanceof Subscription).toBeTruthy();
      });
    });

    describe('isUpdating$', () => {
      it('should be created by selecting isUpdating from profile state', () => {
        const expected = _store.select(fromRoot.getProfileIsUpdating);

        expect(component.isUpdating$).toEqual(expected);
      });
    });

    describe('loadingSpinner', () => {
      it('should be undefined by default', () => {
        expect(component.loadingSpinner).toBeUndefined();
      });
    });
  });

  describe('Methods', () => {
    describe('ionViewWillEnter()', () => {
      it('should subscribe to isUpdating$', () => {
        spyOn(component.isUpdating$, 'subscribe');

        component.ionViewWillEnter();

        expect(component.isUpdating$.subscribe).toHaveBeenCalled();
      });

      it('should add one subscription in total', () => {
        component.ionViewWillEnter();

        expect(component.subscriptions['_subscriptions'].length).toBe(1);
      });
    });

    describe('ionViewWillLeave()', () => {
      it('should unsubscribe from observables', () => {
        spyOn(component.subscriptions, 'unsubscribe');

        component.ionViewWillLeave();

        expect(component.subscriptions.unsubscribe).toHaveBeenCalled();
      });
    });

    describe('sendFeedback()', () => {
      it('should dispatch a new profile.FeedbackAction', () => {
        const fakeFeedback = {
          navigationRating: 5,
          navigationComments: 'Comment about navigation',
          featuresToAdd: 'Comment suggesting new features',
          tipsForImporovement: 'Comment with tips for improvement',
        };
        component.feedbackForm.setValue(fakeFeedback);

        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof profile.FeedbackAction).toBeTruthy();
          expect(action.type).toBe(profile.FEEDBACK);
          expect(action.payload).toEqual(fakeFeedback);
        });
        component.sendFeedback();
      });
    });
  });
});

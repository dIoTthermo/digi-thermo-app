import { AlertOptions } from 'ionic-angular';

export interface IErrorAlert extends AlertOptions {
  originalError?: any;
}

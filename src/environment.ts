interface IEnvironmentConfig {
  firebaseConfig: { [key: string]: string };
}

const environment: { production: boolean } = { production: false };

const DevEnvironmentConfig: IEnvironmentConfig = {
  firebaseConfig: {
    apiKey: 'AIzaSyB4at38akmpDoEgmZi-XrwD68oGrWSx3G8',
    authDomain: 'digi-thermo-b459f.firebaseapp.com',
    databaseURL: 'https://digi-thermo-b459f.firebaseio.com',
    projectId: 'digi-thermo-b459f',
    storageBucket: 'digi-thermo-b459f.appspot.com',
    messagingSenderId: '110354954103'
  }
};

const OpenWeatherConfig = {
  apiUrl: 'https://api.openweathermap.org/data/2.5',
  apiKey: '72baeeb9c2552c9dcf34fa44ca494b93'
};

export { environment, DevEnvironmentConfig, OpenWeatherConfig };

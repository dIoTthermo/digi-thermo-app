import { IErrorAlert } from './../../models/error-alert';

export interface IDataLog {
  $key?: string;
  comment?: string;
  createdAt?: number | Object;
  humidity: number;
  temperature: number;
  title?: string;
  idealTemperature?: number;
}

export interface IDataLogUpdate {
  $key: string;
  updates: {
    comment?: string;
    title?: string;
  };
}

export interface IDataLogDelete {
  $key: string;
}

export interface IFeedbackRating {
  navigationRating: number;
  navigationComments?: string;
  featuresToAdd?: string;
  tipsForImporovement?: string;
}

export interface State {
  isUpdating: boolean;
  temperatureUnit: string;
  refrigerant: string;
  dataLogs: Array<IDataLog>;
  error: IErrorAlert;
}

export const initialState: State = {
  isUpdating: false,
  temperatureUnit: 'F',
  refrigerant: 'R134A',
  dataLogs: [],
  error: null,
};

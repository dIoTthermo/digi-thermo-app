import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Platform, Tabs, Tab } from 'ionic-angular';

import { App } from 'ionic-angular';

import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/concat';

import * as _ from 'lodash';

import { ModalService } from './../../providers/modal.service';
import * as navigation from './navigation.actions';
import { TABS } from './navigation.types';

@Injectable()
export class NavigationEffects {
  @Effect()
  pushOnto$: Observable<Action> = this.actions$
    .ofType(navigation.PUSH_ONTO_STACK)
    .switchMap((action: navigation.PushOntoAction) => {
      const { page, stack } = action.payload;
      return Observable.concat(
        Observable.of(new navigation.ChangeStackAction(stack)),
        Observable.of(new navigation.PushAction(page))
      );
    });

  @Effect({ dispatch: false })
  push$: Observable<Action> = this.actions$
    .ofType(navigation.PUSH)
    .do((action: navigation.PushAction) => {
      const navCtrl = this.app.getActiveNav();
      const page = action.payload.name;
      const isRoot: boolean = _.get(action.payload, 'options.setRoot', false);

      let rootCtrl;
      if (navCtrl instanceof Tab) {
        rootCtrl = (<Tabs>navCtrl.parent).parent;
      } else if (navCtrl instanceof Tabs) {
        rootCtrl = <Tab>navCtrl.parent;
      } else {
        rootCtrl = navCtrl;
      }

      if (isRoot) {
        rootCtrl.setRoot(page);
      } else {
        rootCtrl.push(page);
      }
    });

  @Effect({ dispatch: false })
  pop$: Observable<Action> = this.actions$
    .ofType(navigation.POP)
    .do((action: navigation.PopAction) => {
      const navCtrl = this.app.getActiveNav();

      if (navCtrl) { navCtrl.pop(null, null); }
    });

  @Effect()
  changeTab$: Observable<Action> = this.actions$
    .ofType(navigation.CHANGE_TAB)
    .do((action: navigation.ChangeTabAction) => {
      const navCtrl = this.app.getActiveNav();
      const tabIdx = action.payload;

      navCtrl.parent.select(tabIdx);
    })
    .switchMap((action: navigation.ChangeTabAction) => {
      const stack = TABS[action.payload].stack;
      return Observable.of(new navigation.ChangeStackAction(stack));
    });

  @Effect({ dispatch: false })
  openModal$: Observable<Action> = this.actions$
    .ofType(navigation.OPEN_MODAL)
    .do((action: navigation.OpenModalAction) => {
      this.modalService.openModal(action.payload);
    });

  @Effect({ dispatch: false })
  openBrowser$: Observable<Action> = this.actions$
    .ofType(navigation.OPEN_BROWSER)
    .do((action: navigation.OpenBrowserAction) => {
      this.platform.ready().then(() => {
        this.inAppBrowser.create(action.payload, '_blank', 'location=no');
      });
    });

  constructor(
    private actions$: Actions,
    private app: App,
    private modalService: ModalService,
    private platform: Platform,
    private inAppBrowser: InAppBrowser
  ) {}
}

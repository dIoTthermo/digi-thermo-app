import { TestBed } from '@angular/core/testing';

import { App, NavController } from 'ionic-angular';

import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';

import { Observable } from 'rxjs/Observable';

import * as actions from './sensor.actions';
import * as sensor from './sensor.types';
import * as navActions from '../navigation/navigation.actions';
import { SensorEffects } from './sensor.effects';
import { BluetoothService } from './../../providers/bluetooth.service';
import { BluetoothServiceMock } from './../../mocks/bluetooth.service.mock';

describe('SensorEffects', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      EffectsTestingModule
    ],
    providers: [
      SensorEffects,
      {
        provide: BluetoothService,
        useClass: BluetoothServiceMock,
      },
    ]
  }));

  let runner: EffectsRunner;
  let sensorEffects: SensorEffects;
  let bluetoothService: BluetoothService;

  beforeEach(() => {
    runner = TestBed.get(EffectsRunner);
    sensorEffects = TestBed.get(SensorEffects);
    bluetoothService = TestBed.get(BluetoothService);
  });

  it('should return IS_ENABLED_SUCESS on IS_ENABLED', () => {
    spyOn(bluetoothService, 'isEnabled').and.callThrough();
    runner.queue(new actions.IsBluetoothEnabledAction());

    sensorEffects.isBluetoothEnabled$
      .subscribe(result => {
        expect(result.type).toBe(actions.IS_BLUETOOTH_ENABLED_SUCCESS);
      });

    expect(bluetoothService.isEnabled).toHaveBeenCalled();
  });

  it('should return IS_ENABLED_FAILED on IS_ENABLED', () => {
    spyOn(bluetoothService, 'isEnabled')
      .and.returnValue(Observable.throw({}));
    runner.queue(new actions.IsBluetoothEnabledAction());

    sensorEffects.isBluetoothEnabled$
      .subscribe(result => {
        expect(result.type).toBe(actions.IS_BLUETOOTH_ENABLED_FAILED);
      });

    expect(bluetoothService.isEnabled).toHaveBeenCalled();
  });

  it('should return SCAN_DEVICES when IS_ENABLED_SUCCESS', () => {
    runner.queue(new actions.IsBluetoothEnabledSuccessAction());

    sensorEffects.enabledSuccess$
      .subscribe(result => {
        expect(result.type).toBe(actions.SCAN_DEVICES);
      });
  });

  it('should forceBluetooth() on IS_BLUETOOTH_ENABLED_FAILED', () => {
    spyOn(bluetoothService, 'forceBluetooth');
    runner.queue(new actions.IsBluetoothEnabledFailedAction());

    sensorEffects.enabledFailed$
      .subscribe(() => {});

    expect(bluetoothService.forceBluetooth).toHaveBeenCalled();
  });

  it('should scanDevices() on SCAN_DEVICES', () => {
      spyOn(bluetoothService, 'scanDevices').and.callThrough();
      runner.queue(new actions.ScanDevicesAction());

      sensorEffects.getConnections$
      .subscribe(() => {});

      expect(bluetoothService.scanDevices).toHaveBeenCalled();
  });

  it('should connectToDevice() on CONNECT', () => {
      spyOn(bluetoothService, 'connectToDevice').and.callThrough();
      const payload = { name: 'FakeDevice', id: '1' };
      runner.queue(new actions.ConnectAction(payload));

      sensorEffects.connect$
        .subscribe(() => {});

      expect(bluetoothService.connectToDevice).toHaveBeenCalled();
  });

  it('should return PUSH on IS_CONNECTED', () => {
      runner.queue(new actions.IsConnectedAction(null));

      sensorEffects.isConnected$
      .subscribe(result => {
          expect(result.type).toBe(navActions.PUSH);
      });
  });

  it('should readDevice() on GET_SENSOR_DATA', () => {
    spyOn(bluetoothService, 'readDevice');
    runner.queue(new actions.GetSensorDataAction());

    sensorEffects.getSensorData$
      .subscribe(() => {});

    expect(bluetoothService.readDevice).toHaveBeenCalled();
  });

  it('should disconnectFromDevice() on DISCONNECT', () => {
    spyOn(bluetoothService, 'disconnectFromDevice');
    runner.queue(new actions.DisconnectAction());

    sensorEffects.disconnect$
      .subscribe(() => {});

    expect(bluetoothService.disconnectFromDevice).toHaveBeenCalled();
  });

  it('should setSensorEmittingState() on SET_EMITTING_STATE', () => {
    spyOn(bluetoothService, 'setSensorEmittingState');
    runner.queue(new actions.SetEmittingStateAction(1));

    sensorEffects.setEmittingState$
      .subscribe(() => {});

    expect(bluetoothService.setSensorEmittingState).toHaveBeenCalled();
  });
});

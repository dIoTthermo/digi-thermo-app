import { IErrorAlert } from './../../models/error-alert';

export interface IConnectedDevice {
  characteristics: Array<any>;
  id: String;
  name: String;
  services: Array<any>;
}

export interface IDevice {
  name: String;
  id: String;
}

export interface State {
  isLoading: boolean;
  isEnabled: boolean;
  deviceList: Array<IDevice>;
  isConnecting: boolean;
  isConnected: boolean;
  lastReading: IDeviceReading;
}

export const initialState: State = {
  isLoading: false,
  isEnabled: false,
  deviceList: [],
  isConnecting: false,
  isConnected: false,
  lastReading: null,
};

export interface IDeviceReading {
  temperature: number;
  humidity: number;
}

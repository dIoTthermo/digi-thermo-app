import * as authentication from './authentication.actions';
import { State, initialState, IFirebaseUserProfile } from './authentication.types';
import { IErrorAlert } from './../../models/error-alert';

export { State, initialState };

export function reducer(state = initialState, action: authentication.Actions): State {
  const type = action.type;

  switch (type) {

    case authentication.FACEBOOK_LOGIN:
    case authentication.LOGIN: {
      return {
        ...state,
        isLoading: true
      };
    }

    case authentication.FACEBOOK_LOGIN_SUCCESS:
    case authentication.LOGIN_SUCCESS: {
      const payload = (<authentication.LoginSuccessAction | authentication.FacebookLoginSuccessAction>action).payload;
      return {
        ...state,
        isLoading: false,
        isRegistering: false,
        isAuthenticated: true,
        user: payload,
        error: null,
      };
    }

    case authentication.FACEBOOK_LOGIN_FAILED:
    case authentication.LOGIN_FAILED: {
      const payload = (<authentication.LoginFailedAction | authentication.FacebookLoginFailedAction>action).payload;
      return {
        ...state,
        isLoading: false,
        isRegistering: false,
        error: payload,
      };
    }

    case authentication.LOGOUT: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case authentication.LOGOUT_SUCCESS: {
      return {
        ...initialState
      };
    }

    case authentication.LOGOUT_FAILED: {
      const payload = (<authentication.LogoutFailedAction>action).payload;
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    }

    case authentication.RESET_PASSWORD: {
      const payload = (<authentication.ResetPasswordAction>action).payload;
      return {
        ...state,
        isResettingPassword: true,
      };
    }

    case authentication.RESET_PASSWORD_SUCCESS: {
      const payload = (<authentication.ResetPasswordSuccessAction>action).payload;
      return {
        ...state,
        isResettingPassword: false,
      };
    }

    case authentication.RESET_PASSWORD_FAILED: {
      const payload = (<authentication.ResetPasswordFailedAction>action).payload;
      return {
        ...state,
        isResettingPassword: false,
        error: payload
      };
    }

    case authentication.REGISTER: {
      return {
        ...state,
        isRegistering: true,
      };
    }


    case authentication.REGISTER_FAILED: {
      const payload = (<authentication.RegisterFailedAction>action).payload;
      return {
        ...state,
        isRegistering: false,
        error: payload,
      };
    }

    case authentication.SET_GEOLOCATION: {
      return {
        ...state,
        geolocation: action.payload,
      };
    }

    case authentication.DISMISS_ERROR: {
      return {
        ...state,
        error: null,
      };
    }

    default: {
      return state;
    }
  }
}

export const getIsLoading = (state: State) => state.isLoading;
export const getIsRegistering = (state: State) => state.isRegistering;
export const getIsAuthenticated = (state: State) => state.isAuthenticated;
export const getIsResettingPassword = (state: State) => state.isResettingPassword;
export const getGeoLocation = (state: State) => state.geolocation;
export const getUser = (state: State) => state.user;
export const getError = (state: State) => state.error;

import { TestBed } from '@angular/core/testing';

import { App, NavController } from 'ionic-angular';

import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';

import { Observable } from 'rxjs/Observable';

import * as actions from './authentication.actions';
import * as navActions from '../navigation/navigation.actions';
import * as authentication from './authentication.types';
import { AuthenticationEffects } from './authentication.effects';
import { AuthService } from './../../providers/auth.service';
import { AuthServiceMock } from './../../mocks/auth.service.mock';
import { AngularFireAuthMock } from './../../mocks/angularfire2/angular-fire-auth.mock';

describe('AuthenticationEffects', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      EffectsTestingModule
    ],
    providers: [
      AuthenticationEffects,
      {
        provide: AuthService,
        useClass: AuthServiceMock,
      },
    ]
  }));

  let runner: EffectsRunner;
  let authenticationEffects: AuthenticationEffects;
  let authService: AuthService;

  beforeEach(() => {
    runner = TestBed.get(EffectsRunner);
    authenticationEffects = TestBed.get(AuthenticationEffects);
    authService = TestBed.get(AuthService);
  });

  it('should return LOGIN_SUCCESS action on LOGIN success', () => {
    const email = 'fake@email.com';
    const password = '123test';
    const payload = { email, password };
    spyOn(authService, 'login').and.callThrough();

    runner.queue(new actions.LoginAction(payload));
    authenticationEffects.login$
      .subscribe(result => {
        expect(result.type).toBe(actions.LOGIN_SUCCESS);
      });

    expect(authService.login).toHaveBeenCalledWith(payload);
  });

  it('should return LOGIN_FAILED action on LOGIN error', () => {
    const email = 'invalid@email.com';
    const password = '123test';
    const payload = { email, password };
    spyOn(authService, 'login')
      .and.returnValue(Observable.throw({}));

    runner.queue(new actions.LoginAction(payload));
    authenticationEffects.login$
      .subscribe(result => {
        expect(result.type).toBe(actions.LOGIN_FAILED);
      });

    expect(authService.login).toHaveBeenCalledWith(payload);
  });

  it('should return FACEBOOK_LOGIN_SUCCESS on FACEBOOK_LOGIN success', () => {
    spyOn(authService, 'facebookAuth').and.callThrough();

    runner.queue(new actions.FacebookLoginAction());
    authenticationEffects.facebookLogin$
      .subscribe(result => {
        expect(result.type).toBe(actions.FACEBOOK_LOGIN_SUCCESS);
      });

    expect(authService.facebookAuth).toHaveBeenCalled();
  });

  it('should return FACEBOOK_LOGIN_FAILED on FACEBOOK_LOGIN error', () => {
    spyOn(authService, 'facebookAuth')
      .and.returnValue(Observable.throw({}));

    runner.queue(new actions.FacebookLoginAction());
    authenticationEffects.facebookLogin$
      .subscribe(result => {
        expect(result.type).toBe(actions.FACEBOOK_LOGIN_FAILED);
      });

    expect(authService.facebookAuth).toHaveBeenCalled();
  });

  // This test is supposed to verify whether LOGIN_SUCCESS emits a PUSH action

  it('should output PUSH on LOGIN_SUCCESS or FACEBOOK_LOGIN_SUCCESS', () => {
    const payload = { name: 'ConnectSensorPage', options: { setRoot: true } };

    runner.queue(new actions.LoginSuccessAction(null));
    authenticationEffects.loginSuccess$
      .subscribe(result => {
        expect(result.type).toBe(navActions.PUSH);
        expect(result.payload).toEqual(payload);
      });
  });

  it('should call updateGeolocation on LOGIN_SUCCESS and FACEBOOK_LOGIN_SUCCESS', () => {
    spyOn(authService, 'updateGeolocation');
    runner.queue(new actions.LoginSuccessAction(null));
    runner.queue(new actions.FacebookLoginSuccessAction(null));

    authenticationEffects.loginSuccess$
      .subscribe(() => {});

      expect(authService.updateGeolocation).toHaveBeenCalledTimes(2);
  });

  it('should return LOGOUT_SUCCESS on LOGOUT success', () => {
    spyOn(authService, 'logout').and.callThrough();

    runner.queue(new actions.LogoutAction());
    authenticationEffects.logout$
      .subscribe(result => {
        expect(result.type).toBe(navActions.PUSH);
        expect(result.payload).toEqual({ name: 'LoginPage', options: { setRoot: true } });
      });

    expect(authService.logout).toHaveBeenCalled();
  });

  it('should return LOGOUT_FAILED on LOGOUT error', () => {
    spyOn(authService, 'logout')
      .and.returnValue(Observable.throw({}));

    runner.queue(new actions.LogoutAction());
    authenticationEffects.logout$
      .subscribe(result => {
        expect(result.type).toBe(actions.LOGOUT_FAILED);
      });

    expect(authService.logout).toHaveBeenCalled();
  });

  it('should return RESET_PASSWORD_SUCCESS on RESET_PASSWORD success', () => {
    const email = 'fake@email.com';
    spyOn(authService, 'resetPassword').and.callThrough();

    runner.queue(new actions.ResetPasswordAction(email));
    authenticationEffects.resetPassword$
      .subscribe(result => {
        expect(result.type).toBe(actions.RESET_PASSWORD_SUCCESS);
      });

    expect(authService.resetPassword).toHaveBeenCalledWith(email);
  });

  it('should return RESET_PASSWORD_ERROR on RESET_PASSWORD error', () => {
    const email = 'fake@email.com';
    spyOn(authService, 'resetPassword').and.callThrough()
    .and.returnValue(Observable.throw({}));

    runner.queue(new actions.ResetPasswordAction(email));
    authenticationEffects.resetPassword$
      .subscribe(result => {
        expect(result.type).toBe(actions.RESET_PASSWORD_FAILED);
      });

    expect(authService.resetPassword).toHaveBeenCalledWith(email);
  });

  it('should return POP on RESET_PASSWORD_SUCCESS', () => {
    runner.queue(new actions.ResetPasswordSuccessAction(null));
    authenticationEffects.resetPasswordSuccess$
      .subscribe(result => {
        expect(result.type).toBe(navActions.POP);
      });
  });

  it('should return LOGIN action on REGISTER success', () => {
    const email = 'fake@email.com';
    const password = '123test';
    const payload = { email, password };
    spyOn(authService, 'signUp').and.callThrough();

    runner.queue(new actions.RegisterAction(payload));
    authenticationEffects.register$
      .subscribe(result => {
        expect(result.type).toBe(actions.LOGIN);
      });

    expect(authService.signUp).toHaveBeenCalledWith(payload);
  });

  it('should return REGISTER_FAILED action on REGISTER error', () => {
    const email = 'invalid@email.com';
    const password = '123test';
    const payload = { email, password };
    spyOn(authService, 'signUp')
      .and.returnValue(Observable.throw({}));

    runner.queue(new actions.RegisterAction(payload));
    authenticationEffects.register$
      .subscribe(result => {
        expect(result.type).toBe(actions.REGISTER_FAILED);
      });

    expect(authService.signUp).toHaveBeenCalledWith(payload);
  });

  it('should return LOGIN_SUCCESS action on CHECK_AUTH_STATE success', () => {
    spyOn(authService, 'checkAuthState')
      .and.returnValue(Observable.of(AngularFireAuthMock.FAKE_PROFILE));

    runner.queue(new actions.CheckAuthStateAction());
    authenticationEffects.checkAuthState$
      .subscribe(result => {
        expect(result.type).toBe(actions.LOGIN_SUCCESS);
      });
  });
});

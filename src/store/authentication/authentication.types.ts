import { IErrorAlert } from './../../models/error-alert';

export interface ILoginCredentials {
  email: string;
  password: string;
  confirmPassword?: string;
}

export interface IFirebaseBasicProfile {
  displayName: string | null;
  email: string | null;
  photoURL: string | null;
  providerId: string;
  uid: string;
}

export interface IFirebaseUserProfile extends IFirebaseBasicProfile {
  emailVerified: boolean;
  isAnonymous: boolean;
  providerData: Array<IFirebaseBasicProfile | null>;
  refreshToken: string;
}

export interface State {
  isLoading: boolean;
  isRegistering: boolean;
  isAuthenticated: boolean;
  isResettingPassword: boolean;
  geolocation: IGeolocation;
  user: IFirebaseUserProfile;
  error: IErrorAlert;
}

export interface IGeolocation {
    latitude: number;
    longitude: number;
}

export const initialState: State = {
  isLoading: false,
  isRegistering: false,
  isAuthenticated: false,
  isResettingPassword: false,
  geolocation: null,
  user: null,
  error: null,
};

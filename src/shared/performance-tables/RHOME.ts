import { PerformanceTable, Range } from './performance-table';

/* Performance Table RHOME
 ** Ambient Temperature: C°, Relative Humidity: %, Maximum Left Center Discharge Air Temperature: C°
 */

export const RHOME: Array<PerformanceTable> = [

    /* 13° - 18° C */
    new PerformanceTable(new Range(13, 18), new Range(0, 100), 13),

    /* 19° - 29° C */
    new PerformanceTable(new Range(19, 29), new Range(0, 39), 14),
    new PerformanceTable(new Range(19, 29), new Range(40, 100), 15),

    /* 30° - 46° C */
    new PerformanceTable(new Range(30, 46), new Range(0, 49), 16),
    new PerformanceTable(new Range(30, 46), new Range(50, 100), 18),

    /* 47° - 100° C */
    new PerformanceTable(new Range(47, 100), new Range(0, 49), 19),
    new PerformanceTable(new Range(47, 100), new Range(50, 100), 20)

];

import { PerformanceTable, Range } from './performance-table';

/* Performance Table R134A
 ** Ambient Air Temperature: C°, Relative Humidity: %, Maximum Center Discharge Air Temperature: C°
 */

export const R134A: Array<PerformanceTable> = [

    /* 13° - 18° C */
    new PerformanceTable(new Range(13, 18), new Range(0, 100), 7),

    /* 19° - 24° C */
    new PerformanceTable(new Range(19, 24), new Range(0, 39), 8),
    new PerformanceTable(new Range(19, 24), new Range(40, 100), 9),

    /* 25° - 29° C */
    new PerformanceTable(new Range(25, 29), new Range(0, 34), 9),
    new PerformanceTable(new Range(25, 29), new Range(35, 60), 10),
    new PerformanceTable(new Range(25, 29), new Range(61, 100), 11),

    /* 30° - 35° C */
    new PerformanceTable(new Range(30, 35), new Range(0, 29), 12),
    new PerformanceTable(new Range(30, 35), new Range(30, 50), 13),
    new PerformanceTable(new Range(30, 35), new Range(51, 100), 14),

    /* 36° - 41° C */
    new PerformanceTable(new Range(36, 41), new Range(0, 19), 16),
    new PerformanceTable(new Range(36, 41), new Range(20, 40), 17),
    new PerformanceTable(new Range(36, 41), new Range(41, 100), 18),

    /* 42° - 46° C */
    new PerformanceTable(new Range(42, 46), new Range(0, 19), 17),
    new PerformanceTable(new Range(42, 46), new Range(20, 100), 19),

    /* 47° - 100° C */
    new PerformanceTable(new Range(47, 100), new Range(0, 100), 20)

];

export class Range {

    min_range: number;
    max_range: number;

    constructor(
        min_range: number,
        max_range: number
    ) {
        this.min_range = min_range;
        this.max_range = max_range;
    }

}

export class PerformanceTable {

    ambient_temperature: Range;
    relative_humidity: Range;
    maximum_center_discharge_air_temperature: number;

    constructor(
        ambient_temperature: Range,
        relative_humidity: Range,
        maximum_center_discharge_air_temperature: number) {

        this.ambient_temperature = ambient_temperature;
        this.relative_humidity = relative_humidity;
        this.maximum_center_discharge_air_temperature = maximum_center_discharge_air_temperature;
    }

    private checkTemperature(temperature$) {
        return temperature$ >= this.ambient_temperature.min_range && temperature$ <= this.ambient_temperature.max_range;
    }

    private checkHumidity(humidity$) {
        return humidity$ >= this.relative_humidity.min_range && humidity$ <= this.relative_humidity.max_range;
    }

    getIdealTemperature(temperature$, humidity$): number {
        if (this.checkTemperature(temperature$) && this.checkHumidity(humidity$)){
            return this.maximum_center_discharge_air_temperature;
        }
        return null;
    }

}

import { PerformanceTable, Range } from './performance-table';

/* Performance Table R1234YF
 ** Ambient Temperature: C°, Relative Humidity: %, Maximum Left Center Discharge Air Temperature: C°
 */

export const R1234YF: Array<PerformanceTable> = [

    /* 13° - 18° C */
    new PerformanceTable(new Range(13, 18), new Range(0, 100), 8),

    /* 19° - 24° C */
    new PerformanceTable(new Range(19, 24), new Range(0, 39), 9),
    new PerformanceTable(new Range(19, 24), new Range(40, 100), 10),

    /* 25° - 29° C */
    new PerformanceTable(new Range(25, 29), new Range(0, 34), 11),
    new PerformanceTable(new Range(25, 29), new Range(35, 60), 12),
    new PerformanceTable(new Range(25, 29), new Range(61, 100), 13),

    /* 30° - 35° C */
    new PerformanceTable(new Range(30, 35), new Range(0, 29), 14),
    new PerformanceTable(new Range(30, 35), new Range(30, 50), 15),
    new PerformanceTable(new Range(30, 35), new Range(51, 100), 16),

    /* 36° - 41° C */
    new PerformanceTable(new Range(36, 41), new Range(0, 19), 17),
    new PerformanceTable(new Range(36, 41), new Range(20, 40), 18),
    new PerformanceTable(new Range(36, 41), new Range(41, 100), 19),

    /* 42° - 46° C */
    new PerformanceTable(new Range(42, 46), new Range(0, 19), 18),
    new PerformanceTable(new Range(42, 46), new Range(20, 100), 20),

    /* 47° - 100° C */
    new PerformanceTable(new Range(47, 100), new Range(0, 100), 20)
];

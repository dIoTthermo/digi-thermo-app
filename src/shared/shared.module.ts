import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { MomentModule } from 'angular2-moment';

import { TemperaturePreferencePipe, SortByPipe, RoundNumberPipe } from './shared.pipes';

@NgModule({
  imports: [
    MomentModule,
  ],
  exports: [
    MomentModule,
    TemperaturePreferencePipe,
    SortByPipe,
    RoundNumberPipe,
  ],
  declarations: [
    TemperaturePreferencePipe,
    SortByPipe,
    RoundNumberPipe,
  ],
})
export class SharedModule { }

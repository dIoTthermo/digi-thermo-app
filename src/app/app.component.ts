import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';

import { Platform } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { Store } from '@ngrx/store';
import { Geolocation } from '@ionic-native/geolocation';

import * as fromRoot from './../store/app.reducer';
import * as authentication from './../store/authentication/authentication.actions';

@Component({
  templateUrl: 'app.html'
})
export class AppComponent implements OnInit {
  public rootPage: any = 'LoginPage';

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private store: Store<fromRoot.State>,
    public geolocation: Geolocation
  ) {}

  ngOnInit() {
    this.platform.ready().then(() => {
      this.store.dispatch(new authentication.CheckAuthStateAction());
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.geolocation.getCurrentPosition()
        .then(location => {
          try {
            const { latitude, longitude } = location.coords;
            this.store.dispatch(new authentication.SetGeolocationAction({ latitude, longitude }));
          } catch (e) {
            // No location, doesn't matter.
          }
        })
        .catch(error => {
          // No location, doesn't matter.
        });
    });
  }
}


import { Component } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

import {
  IonicPage,
  Alert,
  AlertController,
  LoadingController,
  Loading
} from 'ionic-angular';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/startWith';

import * as navigation from '../../store/navigation/navigation.actions';
import * as authentication from '../../store/authentication/authentication.actions';
import * as fromRoot from '../../store/app.reducer';
import { ILoginCredentials } from './../../store/authentication/authentication.types';
import { BasePage } from './../base.page';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage extends BasePage {
  public loginForm: FormGroup;
  public disableLoginButton$: Observable<boolean>;
  public subscriptions: Subscription;

  public isLoading$: Observable<boolean>;
  public authError$: Observable<any>;

  public loadingSpinner: Loading;
  public alert: Alert;

  constructor(
    formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    protected store: Store<fromRoot.State>
  ) {
    super(store);

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
      ])],
    });
    this.disableLoginButton$ = this.loginForm.statusChanges
      .map(status => status !== 'VALID')
      .startWith(true);

    this.isLoading$ = store.select(fromRoot.getAuthIsLoading);
    this.authError$ = store.select(fromRoot.getAuthError);
  }

  ionViewWillEnter() {
    this.subscriptions = new Subscription();
    const isLoadingSub = this.isLoading$
      .subscribe(this.handleLoading.bind(this));


    const authErrorSub = this.authError$
      .subscribe(this.handleAuthError.bind(this));

    this.subscriptions.add(isLoadingSub);
    this.subscriptions.add(authErrorSub);
  }

  ionViewWillLeave() {
    this.subscriptions.unsubscribe();
    if (this.alert) { this.alert.dismiss(); }
  }

  public facebookLogin(): void {
    this.store.dispatch(new authentication.FacebookLoginAction());
  }

  public login(): void {
    const credentials: ILoginCredentials = this.loginForm.value;
    this.store.dispatch(new authentication.LoginAction(credentials));
  }

  public forgotPassword(): void {
    this.store.dispatch(new navigation.PushAction({ name: 'ForgotPasswordPage' }));
  }

  public signUp(): void {
    this.store.dispatch(new navigation.PushAction({ name: 'RegisterPage' }));
  }

  public handleLoading(isLoading: boolean): void {
    if (!isLoading && this.loadingSpinner) {
      this.loadingSpinner.dismiss();
    } else if (isLoading && !this.loadingSpinner) {
      this.loadingSpinner = this.loadingCtrl.create();
      this.loadingSpinner.present();
    }
  }

  public handleAuthError(error: any): void {
    if (!error) { return; }

    this.alert = this.alertCtrl.create({
      title: error.title,
      message: error.message,
      buttons: error.buttons
    });

    this.alert.onDidDismiss(() => {
      this.store.dispatch(new authentication.DismissErrorAction());
    });

    this.alert.present();
  }
}

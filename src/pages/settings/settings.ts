import { BluetoothService } from './../../providers/bluetooth.service';
import { Component } from '@angular/core';

import { IonicPage, AlertController } from 'ionic-angular';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { BasePage } from './../base.page';
import * as fromRoot from '../../store/app.reducer';
import * as navigation from '../../store/navigation/navigation.actions';
import * as profile from '../../store/profile/profile.actions';
import * as sensor from '../../store/sensor/sensor.actions';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage extends BasePage {
  public tempUnit$: Observable<any>;
  public refrigerant$: Observable<any>;
  public batteryLevel = 0;

  constructor(
    protected store: Store<fromRoot.State>,
    private bluetooth: BluetoothService
  ) {
    super(store);
    this.tempUnit$ = this.store.select(fromRoot.getProfileTemperatureUnit);
    this.refrigerant$ = this.store.select(fromRoot.getProfileRefrigerant);
  }

  ionViewDidEnter() {
    this.bluetooth.getDeviceBattery().then((res) => { this.batteryLevel = res; });
  }

  public openFeedbackModal() {
    this.store.dispatch(new navigation.OpenModalAction('FeedbackModal'));
  }

  public openHelpPage() {
    this.store.dispatch(new navigation.OpenBrowserAction('https://www.acdiagnosis.com/getting-started'));
  }

  public logout() {
    this.store.dispatch(new sensor.DisconnectAction());
  }

  public changeUnit(newUnit: string) {
    this.store.dispatch(new profile.UpdateTemperatureUnitAction(newUnit));
  }

  public changeRefrigerant(newRefrigerant: string) {
    this.store.dispatch(new profile.UpdateRefrigerantAction(newRefrigerant));
  }

}

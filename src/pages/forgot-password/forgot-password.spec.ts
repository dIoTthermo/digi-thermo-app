import {} from 'jasmine';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { IonicModule, AlertController, LoadingController, Alert, Loading } from 'ionic-angular';
import { StoreModule, Store } from '@ngrx/store';

import { Subscription } from 'rxjs/Subscription';

import { ForgotPasswordPage } from './forgot-password';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';
import { AlertControllerMock } from './../../mocks/ionic/alert-controller.mock';
import { LoadingControllerMock } from './../../mocks/ionic/loading-controller.mock';
import { StoreMock } from './../../mocks/ngrx/store.mock';

import * as authentication from './../../store/authentication/authentication.actions';
import * as navigation from './../../store/navigation/navigation.actions';
import * as fromRoot from './../../store/app.reducer';
import { AuthService } from './../../providers/auth.service';
import { AuthServiceMock } from './../../mocks/auth.service.mock';

describe('Forgot Password', function() {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<ForgotPasswordPage>;
  let de: DebugElement;
  let component: ForgotPasswordPage;

  beforeEach(async(() => {
    TestBed.configureTestingModule({

      declarations: [ ForgotPasswordPage ],
      imports: [
        IonicModule.forRoot(ForgotPasswordPage),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        FormBuilder,
        {
          provide: AlertController,
          useClass: AlertControllerMock,
        },
        {
          provide: LoadingController,
          useClass: LoadingControllerMock,
        },
        { provide: AuthService,
          useClass: AuthServiceMock
        },
        { provide: Store,
          useValue: new StoreMock(initialState)
        }
      ]
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(ForgotPasswordPage);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('Should create resetPassword component', () => {
    expect(component instanceof ForgotPasswordPage).toBeTruthy();
  });

  describe('Properties', () => {
    describe('logo', () => {
      it('should be an image', () => {
        de = fixture.debugElement.query(By.css('#logo'));
      });
    });

    describe('headingText', () => {
      it('should be an h2 element', () => {
        de = fixture.debugElement.query(By.css('#heading'));
      });
    });

    describe('resetPassword form', () => {
      it('should have one input field', () => {
        const controls = Object.keys(component.resetPasswordForm.controls);

        expect(controls.length).toBe(1);
      });
    });

    describe('resetPasswordForm', () => {
      describe('Email Input', () => {
        let formControl: FormControl;
        beforeEach(() => {
          formControl = component.resetPasswordForm.controls['email'] as FormControl;
        });

        it('should exist', () => {
          expect(formControl).toBeDefined();
        });

        it('should be required', () => {
          const errors = formControl.errors || {};
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Submit button', () => {
        beforeEach(() => {
          de = fixture.debugElement.query(By.css('button[type="submit"]'));
        });

        it('should exist', () => {
          expect(de).toBeDefined();
        });

        it('should dispatch a new authentication.resetPasswordAction', () => {
          const fakeCredentials = {
            email: 'fake@email.com'
          };
          expect(component.resetPasswordForm.valid).toBeFalsy();

          component.resetPasswordForm.controls['email'].setValue(fakeCredentials.email);
          expect(component.resetPasswordForm.valid).toBeTruthy();
          spyOn(_store, 'dispatch').and.callFake((action) => {
            expect(action instanceof authentication.ResetPasswordAction).toBeTruthy();
            expect(action.type).toBe(authentication.RESET_PASSWORD);
            expect(action.payload).toEqual(fakeCredentials.email);
          });

          component.resetPassword();
        });
      });

      describe('disableResetPasswordButton$', () => {
        it('should be updated on ResetPassword form status changes', () => {
          const emailInput = component.resetPasswordForm.controls['email'];

          let testCase = 0;
          component.disableResetPasswordButton$.subscribe((disable) => {
            // Initial value
            if (testCase === 0) {
              expect(disable).toBeTruthy();
            }

            // Invalid Form
            if (testCase === 1) {
              expect(disable).toBeTruthy();
            }

            // Valid Form
            if (testCase === 2) {
              expect(disable).toBeFalsy();
            }
          });

          testCase++;
          emailInput.setValue('invalid');

          testCase++;
          emailInput.setValue('valid@email.com');
        });
      });

      describe('isResettingPassword$', () => {
        it('should be created by selecting isResetPasswording from authentication state', () => {
          const expected = _store.select(fromRoot.getAuthIsResettingPassword);

          expect(component.isResettingPassword$).toEqual(expected);
        });
      });

      describe('authError$', () => {
        it('should be created by selecting error from authentication state', () => {
          const expected = _store.select(fromRoot.getAuthError);

          expect(component.authError$).toEqual(expected);
        });
      });

      describe('alert', () => {
        it('should be undefined by default', () => {
          expect(component.alert).toBeUndefined();
        });
      });
    });
  });

  describe('Methods', () => {
    describe('ionViewWillEnter()', () => {
      it('should subscribe to isresetPassword$', () => {
        spyOn(component.isResettingPassword$, 'subscribe');

        component.ionViewWillEnter();

        expect(component.isResettingPassword$.subscribe).toHaveBeenCalled();
      });

      it('should subscribe to authError$', () => {
        spyOn(component.authError$, 'subscribe');

        component.ionViewWillEnter();

        expect(component.authError$.subscribe).toHaveBeenCalled();
      });

      it('should add two subscriptions in total', () => {
        component.ionViewWillEnter();

        expect(component.subscriptions instanceof Subscription).toBeTruthy();
        expect(component.subscriptions['_subscriptions'].length).toBe(2);
      });
    });

    describe('ionViewWillLeave()', () => {
      beforeEach(() => {
        component.ionViewWillEnter();
      });

      it('should unsubscribe from observables', () => {
        spyOn(component.subscriptions, 'unsubscribe');

        component.ionViewWillLeave();

        expect(component.subscriptions.unsubscribe).toHaveBeenCalled();
      });

      it('should dismiss any present alerts', () => {
        component.alert = <Alert>{dismiss() {}};
        spyOn(component.alert, 'dismiss');

        component.ionViewWillLeave();

        expect(component.alert.dismiss).toHaveBeenCalled();
      });
    });

    describe('goBack()', () => {
      it('should dispatch a new navigation.PopAction', () => {
        component.goBack();
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.PopAction).toBeTruthy();
          expect(action.type).toBe(navigation.POP);
        });
      });
    });

    describe('resetPassword()', () => {
      it('should dispatch a new authentication.resetPasswordAction', () => {
        const fakeCredentials = {
          email: 'fake@email.com',
        };
        component.resetPasswordForm.setValue(fakeCredentials);

        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof authentication.ResetPasswordAction).toBeTruthy();
          expect(action.type).toBe(authentication.RESET_PASSWORD);
          expect(action.payload).toEqual(fakeCredentials.email);
        });

        component.resetPassword();
      });
    });

    describe('handleResettingPassword()', () => {
      it('should dismiss any present spinner when state isResettingPassword is false', () => {
        component.loadingSpinner = <Loading>{ dismiss() {} };
        spyOn(component.loadingSpinner, 'dismiss');

        component.handleResettingPassword(false);

        expect(component.loadingSpinner.dismiss).toHaveBeenCalled();
      });
    });

    describe('handleAuthError()', () => {
      it('should create an alert when passed an error', () => {
        const fakeError = {
          title: 'Alert!',
          message: 'Random Message',
          buttons: ['Fake', 'Buttons']
        };
        spyOn(component.alertCtrl, 'create').and.callThrough();

        component.handleAuthError(fakeError);

        expect(component.alertCtrl.create).toHaveBeenCalled();
      });

      it('should clear the error state once the alert has been dismissed', () => {
        const fakeError = {
          title: 'Alert!',
          message: 'Random Message',
          buttons: ['Fake', 'Buttons']
        };
        spyOn(_store, 'dispatch').and.callFake(action => {
          expect(action instanceof authentication.DismissErrorAction).toBeTruthy();
          expect(action.type).toBe(authentication.DISMISS_ERROR);
        });

        component.handleAuthError(fakeError);
        component.alert.dismiss();
      });
    });
  });
});

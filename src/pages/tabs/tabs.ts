import { SettingsPage } from './../settings/settings';
import { Component, ViewChild } from '@angular/core';

import { IonicPage, Tab, Tabs, AlertController, AlertOptions, Alert, NavController } from 'ionic-angular';

import { Store } from '@ngrx/store';
import { Storage } from '@ionic/storage';

import { BasePage } from './../base.page';
import { TABS } from './../../store/navigation/navigation.types';
import * as fromRoot from './../../store/app.reducer';
import * as navigation from './../../store/navigation/navigation.actions';
import * as $ from 'jquery';
import { SettingsModule } from '../settings/settings.module';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
@IonicPage()
export class TabsPage extends BasePage {
  public tabs = TABS;
  public isFirstLoad = true;
  adviseAlert: Alert;

  constructor(
    protected store: Store<fromRoot.State>,
    private alertCtrl: AlertController,
    private storage: Storage,
    private navCtrl: NavController
  ) {
    super(store);
  }

  ionViewDidLoad() {
    super.ionViewDidLoad();
  }

  public onTabChange(tab: Tab) {

    if (this.isFirstLoad) {
      if (!localStorage.getItem('refrigerantAlertShowed')) {
        this.showRefrigerantAdvise();
      }
      this.isFirstLoad = false;
      return;
    }

    const stack = TABS[tab.index].stack;
    this.store.dispatch(new navigation.ChangeStackAction(stack));
  }

  showRefrigerantAdvise() {
    const message = 'Visit acdiagnosis.com to get started';
    const options: AlertOptions = {
      subTitle: 'Select the type of A/C system you want to test the performance of',
      message,
      cssClass: 'alert_white_message',
      buttons: [
        {
          text: 'Okay'
        }
      ],
    };
    this.adviseAlert = this.alertCtrl.create(options);
    this.adviseAlert.present();
    this.adviseAlert.onDidDismiss(() => {
      localStorage.setItem('refrigerantAlertShowed', 'true');
      setTimeout(function () { document.getElementById("tab-t0-2").click(); }, 100);
    })
  }

}

import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { IonicModule } from 'ionic-angular';

import { StoreModule, Store } from '@ngrx/store';

import { ConnectSensorPage } from './connect-sensor';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as sensor from './../../store/sensor/sensor.actions';
import * as fromRoot from './../../store/app.reducer';

describe('ConnectSensorPage', () => {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<ConnectSensorPage>;
  let de: DebugElement;
  let component: ConnectSensorPage;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectSensorPage],
      imports: [
        IonicModule.forRoot(ConnectSensorPage),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        }
      ],
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(ConnectSensorPage);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('should be created', () => {
    expect(component instanceof ConnectSensorPage).toBeTruthy();
  });

  describe('Properties', () => {
    describe('isConnecting$', () => {
      it('should be created by selecting isConnecting from connect-sensor state', () => {
        const expected = _store.select(fromRoot.getSensorIsConnecting);

        expect(component.isConnecting$).toEqual(expected);
      });
    });

    describe('deviceList$', () => {
      it('should be created by selecting deviceList from connect-sensor state', () => {
        const expected = _store.select(fromRoot.getSensorDeviceList);

        expect(component.deviceList$).toEqual(expected);
      });
    });
  });

  describe('Methods', () => {
    describe('ionViewWillEnter()', () => {
      it('should dispatch IsBluetoothEnabledAction', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof sensor.IsBluetoothEnabledAction).toBeTruthy();
        });

        component.ionViewWillEnter();
      });
    });

    describe('connectSensor()', () => {
      it('should dispatch a new ConnectAction', () => {
        const fakeDevice = {
          id: '193fi045n499fn3o4398943jg42',
          name: 'Digi Thermo Device'
        };

        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof sensor.ConnectAction).toBeTruthy();
          expect(action.type).toBe(sensor.CONNECT);
          expect(action.payload).toEqual(fakeDevice);
        });

        component.connectSensor(fakeDevice);
      });
    });
  });

  describe('Template Integration', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should display a reminder when not connecting', () => {
      de = fixture.debugElement.query(By.css('#reminder'));

      expect(de).toBeTruthy();
    });

    it('should display connecting to sensor text & spinner when connecting', () => {
      (<any>_store).next({
        sensor: { isConnecting: true }
      });

      fixture.detectChanges();
      de = fixture.debugElement.query(By.css('h2'));
      const connectingSpinner = fixture.debugElement.query(By.css('ion-spinner'));

      expect((<HTMLElement>(de.nativeElement)).innerText).toEqual('Connecting to sensor');
      expect(connectingSpinner).toBeTruthy();
    });

    it('should display a list devices to connect to', () => {
      (<any>_store).next({
        sensor: { deviceList:
          [
            { id: '1', name: 'Fake Device 1' },
            { id: '2', name: 'Fake Device 2' },
            { id: '3', name: 'Fake Device 3' }
          ]
        }
      });

      fixture.detectChanges();
      const buttons = fixture.debugElement.queryAll(By.css('ion-content button'));

      expect(buttons.length).toBe(3);
    });

    it('should call connectSensor() with device info when selecting a device', () => {
      const fakeDevices = [
        { id: '1', name: 'Fake Device 1' },
        { id: '2', name: 'Fake Device 2' },
        { id: '3', name: 'Fake Device 3' }
      ];
      (<any>_store).next({
        sensor: { deviceList: fakeDevices }
      });
      spyOn(component, 'connectSensor');

      fixture.detectChanges();
      const buttons = fixture.debugElement.queryAll(By.css('ion-content button'));

      expect(buttons.length).toBe(3);

      buttons.forEach((button, idx) => {
        button.triggerEventHandler('click', null);

        expect(component.connectSensor).toHaveBeenCalledWith(fakeDevices[idx]);
      });
    });
  });
});

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { OpenWeatherConfig } from '../environment';
import { AlertOptions, AlertController, Alert, Loading, LoadingController } from 'ionic-angular';
import { PerformanceTable } from '../shared/performance-tables/performance-table';
import { R134A } from './../shared/performance-tables/R134A';
import { R1234YF } from './../shared/performance-tables/R1234YF';
import { RHOME } from './../shared/performance-tables/RHOME';
import { isNullOrUndefined } from 'util';
import 'rxjs/add/operator/toPromise';

const apiKey: string = OpenWeatherConfig.apiKey;

@Injectable()
export class WeatherService {

    private geolocationAlert: Alert;
    private performanceTable: Array<PerformanceTable>;

    constructor(
        private http: Http,
        private alertCtrl: AlertController,
        public loadingCtrl: LoadingController
    ) { }

    getPosition(): Promise<any> {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
                resp => {
                    resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
                },
                err => {
                    const options: AlertOptions = {
                        title: 'No support for geolocation',
                        subTitle: 'You did not grant access to your location. Go to settings and enable geolocation.',
                        buttons: [{ text: 'Okay' }]
                    };
                    this.geolocationAlert = this.alertCtrl.create(options);
                    this.geolocationAlert.present();
                    reject(err);
                });
        });
    }

    getCurrentWeather(lat: number, lon: number) {
        return this.http.get(`${OpenWeatherConfig.apiUrl}/weather?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric`).toPromise();
    }

    getIdealTemperature(refrigerant: string): Promise<number> {


        switch (refrigerant) {
            case 'R1234YF':
                this.performanceTable = R1234YF;
                break;
            case 'R134A':
                this.performanceTable = R134A;
                break;
            case 'HOME':
                this.performanceTable = RHOME;
                break;
            default:
                this.performanceTable = R134A;
                break;
        }

        return this.getPosition().then(coords => {
            return this.getCurrentWeather(coords.lat, coords.lng).then(weather => {
                const data = JSON.parse(weather['_body']);

                const temperature = Math.trunc(data.main.temp);
                const humidity = Math.trunc(data.main.humidity);

                const performance = this.performanceTable
                    .find((element) =>
                        temperature >= element.ambient_temperature.min_range
                        && temperature <= element.ambient_temperature.max_range
                        && humidity >= element.relative_humidity.min_range
                        && humidity <= element.relative_humidity.max_range);

                if (!isNullOrUndefined(data) && !isNullOrUndefined(performance)) {
                    return performance.getIdealTemperature(temperature, humidity);
                }

                return null;
            });
        });
    }
}

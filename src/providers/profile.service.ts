import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/from';

import * as firebase from 'firebase';
import * as fromRoot from './../store/app.reducer';
import { IDataLog, IDataLogUpdate, IDataLogDelete } from './../store/profile/profile.types';
import { IFeedbackRating } from './../store/profile/profile.types';

@Injectable()
export class ProfileService {
  public dataLogs$: FirebaseListObservable<Array<IDataLog>>;
  public feedback$: FirebaseListObservable<Array<IFeedbackRating>>;
  public profile$: FirebaseObjectObservable<any>;
  public temperatureUnitAF$: FirebaseObjectObservable<any>;
  public refrigerantAF$: FirebaseObjectObservable<any>;

  constructor(
    private af: AngularFireDatabase,
    private store: Store<fromRoot.State>
  ) {
    store.select(fromRoot.getAuthUser)
      .subscribe(user => {
        if (user) {
          this.dataLogs$ = af.list(`dataLogs/${user.uid}`);
          this.feedback$ = af.list(`feedback/${user.uid}`);
          this.profile$ = af.object(`profiles/${user.uid}`);
          this.temperatureUnitAF$ = af.object(`profiles/${user.uid}/temperatureUnit`);
          this.refrigerantAF$ = af.object(`profiles/${user.uid}/refrigerant`);
        }
      });
  }

  public updateUnit(temperatureUnit: string): Observable<any> {
    const userPreference = <Promise<any>>this.profile$.update({ temperatureUnit });

    return Observable.fromPromise(userPreference)
      .catch(error => {
        const updatePreferenceError: { [key: string]: any } = {
          title: 'There was a problem updating your measurement preference',
          message: '',
          buttons: ['Ok'],
          originalError: error,
        };
        return Observable.throw(updatePreferenceError);
      });
  }

  public getUnitFromFirebase(): Observable<any> {
    return <FirebaseObjectObservable<any>>this.temperatureUnitAF$;
  }

  public updateRefrigerant(refrigerant: string): Observable<any> {
    const userPreference = <Promise<any>>this.profile$.update({ refrigerant });

    return Observable.fromPromise(userPreference)
      .catch(error => {
        const updatePreferenceError: { [key: string]: any } = {
          title: 'There was a problem updating your refrigerant preference',
          message: '',
          buttons: ['Ok'],
          originalError: error,
        };
        return Observable.throw(updatePreferenceError);
      });
  }

  public getRefrigerantFromFirebase(): Observable<any> {
    return <FirebaseObjectObservable<any>>this.refrigerantAF$;
  }

  public getDataLogs(): Observable<any> {
    return this.dataLogs$
      .catch(error => {
        const getDataLogsError: { [key: string]: any } = {
          title: 'Server Error',
          message: 'Unable to retrieve your data logs. Please try again.',
          buttons: ['Ok'],
          originalError: error
        };

        return Observable.throw(getDataLogsError);
      });
  }

  public createDataLog(data: IDataLog) {
    const dataLog: IDataLog = Object.assign({}, data, { createdAt: firebase.database.ServerValue.TIMESTAMP });

    const createPromise = this.dataLogs$.push(dataLog);
    return Observable.fromPromise(Promise.resolve(createPromise))
      .catch(error => {
        const createError: { [key: string]: any } = {
          title: 'Create Error',
          message: 'Unable to create your data. Please try again.',
          buttons: ['Ok'],
          originalError: error
        };

        return Observable.throw(createError);
      });
  }

  public updateDataLog(dataLog: IDataLogUpdate) {
    const updatePromise = <Promise<any>>this.dataLogs$.update(dataLog.$key, dataLog.updates);
    return Observable.fromPromise(updatePromise)
      .catch(error => {
        const updateError: { [key: string]: any } = {
          title: 'Update Error',
          message: 'Unable to update your data. Please try again.',
          buttons: ['Ok'],
          originalError: error
        };

        return Observable.throw(updateError);
      });
  }

  public deleteDataLog(dataLog: IDataLogDelete) {
    const deletePromise = <Promise<any>>this.dataLogs$.remove(dataLog.$key);
    return Observable.fromPromise(deletePromise)
      .catch(error => {
        const deleteError: { [key: string]: any } = {
          title: 'Delete Error',
          message: 'Unable to delete your data. Please try again.',
          buttons: ['Ok'],
          originalError: error
        };

        return Observable.throw(deleteError);
      });
  }

  public sendFeedback(feedbackRating: IFeedbackRating): Observable<IFeedbackRating> {
    const feedbackPromise = this.feedback$.push(feedbackRating);

    return Observable.fromPromise(Promise.resolve(feedbackPromise))
      .catch(error => {
        const submitFeedbackError: { [key: string]: any } = {
          title: 'There was sending your feedback. Please try again.',
          message: '',
          buttons: ['Ok'],
          originalError: error,
        };
        return Observable.throw(submitFeedbackError);
      });
  }
}

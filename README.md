# AC diagnosis Mobile App

## Dev
- Clone the repo
- `npm install`
- `ionic serve`
- For the first time, initialize git flow
    - `git flow init`
- Start your feature branches
- Code
- PR your changes into develop for code review and Pipeline test-running.
- ????
- Profit!!

## Build Android
- cordova build --release android
- jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore digithermo.keystore /Users/damarruiz/Desktop/ac-diagnosis-app/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk digithermo
- /Users/damarruiz/Library/Android/sdk/build-tools/28.0.3/zipalign -v 4 /Users/damarruiz/Desktop/ac-diagnosis-app/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk AC_diagnosis.apk



## Architecture:

### [Angular 2 Firebase](https://github.com/angular/angularfire2)
### [@ngrx library](https://github.com/ngrx):
- [@ngrx/store](https://github.com/ngrx/store)
- [@ngrx/effects](https://github.com/ngrx/effects)
- [@ngrx/store-devtools](https://github.com/ngrx/store-devtools) (Get [Redux Devtools Extension](http://zalmoxisus.github.io/redux-devtools-extension))